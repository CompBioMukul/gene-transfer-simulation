#generate species tree --> java -jar sagephy-1.0.0.jar HostTreeGen [options] [time interval] [birth rate] [death rate] [out prefix] 
java -jar ../sagephy-1.0.0.jar HostTreeGen -min 50 -max 50 -bi 1.0 5.0 2.0 species

#generate gene tree --> java -jar sagephy-1.0.0.jar GuestTreeGen [options] [species tree] [dup rate] [loss rate] [trans rate] [out prefix]
java -jar ../sagephy-1.0.0.jar GuestTreeGen -rt 1.0 ./speciesTree/species.pruned.tree 0.8 0.6 0.7 gene

#generate domian tree --> java -jar sagephy-1.0.0.jar DomainTreeGen [options] [species tree] [gene tree directory] [dup rate] [loss rate] [trans rate] [out prefix]
#generates domain trees with varying interspecies trasfer (-is) of 0.2, 0.5, and 0.8
java -jar ../sagephy-1.0.0.jar DomainTreeGen -rt 1.0 -ig 0 -is 0.2 speciesTree/species.pruned.tree prunedGeneTree/ 0 0 0.7 domain_0.2IS
java -jar ../sagephy-1.0.0.jar DomainTreeGen -rt 1.0 -ig 0 -is 0.5 speciesTree/species.pruned.tree prunedGeneTree/ 0 0 0.7 domain_0.5IS
java -jar ../sagephy-1.0.0.jar DomainTreeGen -rt 1.0 -ig 0 -is 0.8 speciesTree/species.pruned.tree prunedGeneTree/ 0 0 0.7 domain_0.8IS
