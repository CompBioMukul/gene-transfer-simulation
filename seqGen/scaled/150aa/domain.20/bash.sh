#generate sequences --> partial.py [-h] [-dl DOMAIN_LENGTH] [-gl GENE_LENGTH] domain_dir geneTree_dir leafmap_dirs
python3 ~/partial.py -dl 90 -gl 450 -s 3 -ap /home/suz11001/sept30/domain_0.2IS/domainTree /home/suz11001/sept30/prunedGeneTree /home/suz11001/sept30/domain_0.2IS/mapping
mv seqs/ domain_0.2IS/
python3 ~/partial.py -dl 90 -gl 450 -s 3 -ap /home/suz11001/sept30/domain_0.5IS/domainTree /home/suz11001/sept30/prunedGeneTree /home/suz11001/sept30/domain_0.5IS/mapping
mv seqs/ domain_0.5IS/
python3 ~/partial.py -dl 90 -gl 450 -s 3 -ap /home/suz11001/sept30/domain_0.8IS/domainTree /home/suz11001/sept30/prunedGeneTree /home/suz11001/sept30/domain_0.8IS/mapping
mv seqs/ domain_0.8IS/
